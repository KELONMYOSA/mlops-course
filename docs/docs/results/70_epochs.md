## F1

![f1](img/70_epochs/F1_curve.png)

## Precision-Recall

![pr](img/70_epochs/PR_curve.png)

## Results

![results](img/70_epochs/results.png)

## Normalized confusion matrix

![matrix](img/70_epochs/confusion_matrix_normalized.png)
