## F1

![f1](img/120_epochs/F1_curve.png)

## Precision-Recall

![pr](img/120_epochs/PR_curve.png)

## Results

![results](img/120_epochs/results.png)

## Normalized confusion matrix

![matrix](img/120_epochs/confusion_matrix_normalized.png)
