## F1

![f1](img/10_epochs/F1_curve.png)

## Precision-Recall

![pr](img/10_epochs/PR_curve.png)

## Results

![results](img/10_epochs/results.png)

## Normalized confusion matrix

![matrix](img/10_epochs/confusion_matrix_normalized.png)
