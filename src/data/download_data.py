import os

import click
from kaggle.api.kaggle_api_extended import KaggleApi


@click.command()
@click.argument("dataset")
@click.argument("download_path")
def download_kaggle_dataset(dataset: str, download_path: str):
    """Download dataset from Kaggle to specified download path."""
    if not os.path.exists(download_path):
        os.makedirs(download_path)

    api = KaggleApi()
    api.authenticate()

    print(f"Downloading dataset {dataset} to {download_path}")
    api.dataset_download_files(dataset, path=download_path, unzip=True)
    print(f"Dataset {dataset} downloaded and unzipped to {download_path}")


if __name__ == "__main__":
    download_kaggle_dataset()
