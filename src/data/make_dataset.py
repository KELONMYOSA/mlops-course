import json
import os
import shutil

import click


def _generate_data_yaml(save_path: str, train_path: str, val_path: str, label_map: dict):
    data_yaml_content = (
        f"train: {os.path.abspath(train_path)}\n" f"val: {os.path.abspath(val_path)}\n\n" "names:\n" "  0: _?_\n"
    )

    for key, value in label_map.items():
        data_yaml_content += f"  {value}: {key}\n"

    with open(os.path.join(save_path, "trafic_signs.yaml"), "w") as f:
        f.write(data_yaml_content)


def _convert_annotations(dataset_path: str, save_path: str, annotations: dict):
    for annotation in annotations["images"]:
        image_id = annotation["id"]
        file_name = annotation["file_name"]
        width = annotation["width"]
        height = annotation["height"]

        yolo_annotations = []

        for anno in annotations["annotations"]:
            if anno["image_id"] == image_id:
                category_id = anno["category_id"]
                bbox = anno["bbox"]

                x_center = (bbox[0] + bbox[2] / 2) / width
                y_center = (bbox[1] + bbox[3] / 2) / height
                w = bbox[2] / width
                h = bbox[3] / height

                yolo_annotations.append(f"{category_id} {x_center} {y_center} {w} {h}")

        annotation_file = os.path.join(save_path, os.path.splitext(os.path.basename(file_name))[0] + ".txt")
        with open(annotation_file, "w") as f:
            f.write("\n".join(yolo_annotations))

        image_src = os.path.join(dataset_path, "rtsd-frames", file_name)
        image_dst = os.path.join(save_path, os.path.basename(file_name))
        if os.path.exists(image_src):
            shutil.copyfile(image_src, image_dst)


@click.command()
@click.argument("dataset_path")
@click.argument("save_path")
def main(dataset_path: str, save_path: str):
    """Main function to process annotations and generate YAML file."""
    train_path = os.path.join(save_path, "train")
    val_path = os.path.join(save_path, "val")

    with open(os.path.join(dataset_path, "label_map.json")) as f:
        label_map = json.load(f)

    with open(os.path.join(dataset_path, "train_anno_reduced.json")) as f:
        train_anno = json.load(f)

    with open(os.path.join(dataset_path, "val_anno.json")) as f:
        val_anno = json.load(f)

    os.makedirs(train_path, exist_ok=True)
    os.makedirs(val_path, exist_ok=True)

    _convert_annotations(dataset_path, train_path, train_anno)
    _convert_annotations(dataset_path, val_path, val_anno)

    _generate_data_yaml(save_path, train_path, val_path, label_map)


if __name__ == "__main__":
    main()
