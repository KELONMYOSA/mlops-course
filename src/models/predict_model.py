import datetime
import os.path

import click
import torch
from ultralytics import YOLO

device = "cuda:0" if torch.cuda.is_available() else "cpu"


def _init_model(model_path: str) -> YOLO:
    model = YOLO(model_path)
    model.to(device)
    return model


@click.command()
@click.argument("model_path")
@click.argument("img_path")
@click.option(
    "--save_dir",
    default="reports/figures/yolo_prediction_results",
    help="Directory to save the prediction results",
)
@click.option("--conf", default=0.5, help="Confidence threshold for predictions")
@click.option("--imgsz", default=1280, help="Image size for predictions")
def main(model_path: str, img_path: str, save_dir: str, conf: float, imgsz: int):
    """Script for running YOLO model predictions on an image."""
    save_path = os.path.join(save_dir, f"{datetime.datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S')}.jpg")

    model = _init_model(model_path)
    result = model.predict(img_path, conf=conf, imgsz=imgsz, device=device)[0]

    names = []
    detection_count = result.boxes.shape[0]
    for i in range(detection_count):
        cls = int(result.boxes.cls[i].item())
        names.append(result.names[cls])
    print(f"Labels: {', '.join(names)}")

    result.save(filename=save_path)


if __name__ == "__main__":
    main()
