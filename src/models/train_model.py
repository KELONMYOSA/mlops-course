import datetime
import os

import click
import mlflow
import torch
from dotenv import load_dotenv
from ultralytics import YOLO, settings

device = "cuda:0" if torch.cuda.is_available() else "cpu"

# Enabling MLflow
load_dotenv()
mlflow_tracking_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(mlflow_tracking_uri)
settings.update({"mlflow": True})


@click.command()
@click.argument("dataset_path")
@click.option("--epochs", default=1, help="Number of epochs for training")
@click.option("--imgsz", default=1280, help="Image size for training")
def main(dataset_path: str, epochs: int, imgsz: int):
    """Train YOLO model on the given dataset and save the weights."""
    run_name = datetime.datetime.utcnow().strftime("%Y-%m-%d_%H-%M-%S")
    model = YOLO("models/yolo/yolov8n.pt")
    model.to(device)
    model.train(data=dataset_path, project="YOLOv8", name=run_name, epochs=epochs, imgsz=imgsz, device=device)
    model.val(device=device)


if __name__ == "__main__":
    main()
