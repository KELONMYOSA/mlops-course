### Experimental results

| model  | epoch num | time | optimizer | lr   | momentum | box  | f1 score |
|--------|-----------|------|-----------|------|----------|------|----------|
| yolov8 | 10        | 2.5ч | SGD       | 0.01 | 0.937    | 7.5  | 0.78     |
| yolov8 | 10        | 2.5ч | SGD       | 0.01 | 0.937    | 0.05 | 0.57     |
| yolov8 | 70        | 17.5 | SGD       | 0.01 | 0.937    | 7.5  | 0.88     |
| yolov8 | 120       | 30ч  | SGD       | 0.01 | 0.937    | 7.5  | 0.90     |
