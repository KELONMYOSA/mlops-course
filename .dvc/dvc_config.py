import os

from dotenv import load_dotenv

# Загрузка переменных окружения из .env файла
load_dotenv()

# Получение значений переменных
access_key_id = os.getenv("S3_ACCESS_KEY_ID")
secret_access_key = os.getenv("S3_SECRET_ACCESS_KEY")

# Конфигурация DVC
os.system(f"dvc remote modify storage access_key_id {access_key_id}")
os.system(f"dvc remote modify storage secret_access_key {secret_access_key}")
